<?php
error_reporting(1);
$sPROCEESSPIDFILE         = 'lftp.pid';                             //process id file name
$sPROCEESSERRORLOGFILE    = 'lftp_error.log';                       //process error log file
$sPROCESSSTATE            = "RUNNING";                              //helps to control the retriggering of the function which checks if the process is still running

$fnc = isset($_REQUEST['fnc']) ? $_REQUEST['fnc'] : '';             //gets the main function to be called

//ajax is used to get the ftp subfolders, thereby reloading the page
//the ajaxCall flag is set to true each time the ajax occurs, then we reset the function-variable
if(isset($_REQUEST['ajaxCall'])){
    if ($_REQUEST['ajaxCall'] == 'true'){
        $fnc = '';
    }
}

$ftp_user = isset($_REQUEST['ftp_user']) ? $_REQUEST['ftp_user'] : '';
$ftp_passwd = isset($_REQUEST['ftp_passwd']) ? $_REQUEST['ftp_passwd'] : '';
$ftp_server = isset($_REQUEST['ftp_server']) ? $_REQUEST['ftp_server'] : '';
$ftp_path = isset($_REQUEST['ftp_path']) ? $_REQUEST['ftp_path'] : '';
$local_path = isset($_REQUEST['local_path']) ? $_REQUEST['local_path'] : getcwd();

//set the checked radio button
$push_direction = '';
$pull_direction = 'checked="checked"';
if(isset($_REQUEST['transfer_direction'])){
    if($_REQUEST['transfer_direction'] == 'push'){
        $push_direction = 'checked="checked"';
        $pull_direction = '';
    }else{
        $push_direction = '';
        $pull_direction = 'checked="checked"';
    }
}

$sftp_connection = 'checked="checked"';
$ftp_connection = '';
if(isset($_REQUEST['connection_type'])){
    if($_REQUEST['connection_type'] == 'sftp_conn_type'){
        $sftp_connection = 'checked="checked"';
        $ftp_connection = '';
    }else{
        $sftp_connection = '';
        $ftp_connection = 'checked="checked"';
    }
}

//set the checked checkbox
$delete_script = '';
if(isset($_REQUEST['settings_delete_script'])){
    $delete_script = 'checked="checked"';
}


/**
 * Checks the status of the last lftp command
 * @param $fPidFile string file containing the process id
 * @param bool $bOutputCommand true, if the command is to be printed on the screen
 * @param bool $bOutputCommandResult true, if the rersult is to be printed on the screen
 * @return bool, true if process is still running
 */
function getLastRunStatus($fPidFile, $bOutputCommand = false, $bOutputCommandResult = true)
{
    if (file_exists(dir(__FILE__) . $fPidFile)){
        $dOldPid = file_get_contents(dir(__FILE__) . $fPidFile);
        $aOutProcessRunning = array();
        $cmd = "ps -p $dOldPid";
        exec ($cmd, $aOutProcessRunning,$ret);

        //show command
        if($bOutputCommand) {
            echo "<p class='cmd'>$cmd</p>";
        }

        //show command result
        if($bOutputCommandResult) {
            echo htmlentities(implode(PHP_EOL,$aOutProcessRunning), ENT_QUOTES, 'UTF-8');
        }

        //check if process is still running
        if(count($aOutProcessRunning) > 1){
            return true;
        }

    }
    return false;
}


/**
 * checks if the transfer was successful
 * @param $sErrorLogFile string erro lod filename
 * @return bool true on success or if no process was found
 */
function isSuccessful($sErrorLogFile)
{
    $ret = true;
    if (file_exists(dir(__FILE__) . $sErrorLogFile)) {
        $logError = file_get_contents(dir(__FILE__) . $sErrorLogFile);
        if (trim($logError) == '') {
            echo "<p class='success'>Lftp-Transfer finished successfully.</p>";
        } else {
            $ret = false;
            echo "<p class='error'>Lftp-Transfer NOT successful -  See log below</p>";
            echo "<div class='log'>" . $logError. " </div>";
        }
    }

    return $ret;
}


/**
 * Gets the list of folders from the root directory
 * to the given path (directory) plus sub folder level
 * @param $ftp_user string ftp user
 * @param $ftp_passwd string ftp password
 * @param $ftp_server string ftp server
 * @param $path string the main path / directory
 * @return array
 */
function getFTPDirContent($ftp_user, $ftp_passwd, $ftp_server, $path)
{
    // Verbindung aufbauen
    $black_list = array (".", "..", ".local", ".config",  ".ssh", ".git", ".bash_history");
    $conn_id = ftp_connect($ftp_server);

    // Login mit Benutzername und Passwort
    $login_result = ftp_login($conn_id, $ftp_user, $ftp_passwd);

    if($login_result){
        //get the folders we need the first subfolder
        $last = ".";
        $folderLevels = array($last); //main
        if(trim($path) != ''){
            $folderParts = explode("/", $path);
            foreach($folderParts as $folderName){
                if(trim($folderName) != ''){
                    $folderLevels[] = $last . '/' . $folderName;
                    $last = $last . '/' . $folderName;
                }
            }
        }

        //get directory names
        $firstLevelDir= array();
        foreach($folderLevels as $folderName){
            //Inhalt des aktuellen Verzeichnis auslesen
            $contents = ftp_nlist($conn_id, $folderName);  //just one folder level
            foreach($contents as $file){
                $isfile = ftp_size($conn_id, ltrim($file , "."));
                if($isfile == "-1") {
                    if(! in_array($file, $black_list)) {
                        $firstLevelDir[] = ltrim($folderName . '/'. $file . '/', ".");
                    }
                }
            }
        }

        return ($firstLevelDir);
    }
    return array();
}


/**
 * Gets the list of subfolders in the given directory
 * @param $dir string the main directory
 * @param array $results array the list
 * @return array
 */
function getLocalDirContents($dir, &$results = array())
{
    $files = scandir($dir);

    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if(!is_dir($path)) {
            //$results[] = $path;
        } else if($value != "." && $value != "..") {
            getLocalDirContents ($path, $results);
            $results[] = $path;
        }
    }

    $results[] = $dir;
    return $results;
}


/**
 * deletes the given files from the current working folder
 * @param $aFilename array
 */
function deleteFile($aFilename)
{
    foreach ($aFilename as $filename){
        $file = rtrim(getcwd(), DIRECTORY_SEPARATOR ) . DIRECTORY_SEPARATOR. $filename;
        if(file_exists($file)){
            unlink($file);
        }
    }
}


/**
 * Get script location
 * @return string
 */
function getLocation()
{
    $country = '';
    $city = '';
    try{
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
        $country = $geo["geoplugin_countryName"];
        $city = $geo["geoplugin_city"];
    }catch (Exception $ex){
    }

    if($country != ''){
        return " in $country, $city";
    }
    return "";

}


/**
 * Get the Output system
 * @param $sPROCEESSERRORLOGFILE
 * @return string
 */
function getOutput( $sPROCEESSERRORLOGFILE)
{
    //background run: log the error
    return " 1> /dev/null 2> " . $sPROCEESSERRORLOGFILE;
}


/**
 * Port for the given connection type
 * @param $connection_type string|null
 * @return int
 */
function getPort($connection_type)
{
    if(!isset($connection_type)){
        return 22;
    }
    if ($connection_type == 'sftp_conn_type'){
        return 22;
    }
    return 21;
}


/**
 * Returns the string for the given connection type
 * @param $connection_type string|null
 * @return string sftp or ftp
 */
function getConnectionString($connection_type)
{
    if(!isset($connection_type)){
        return ' sftp';
    }
    if ($connection_type == 'sftp_conn_type'){
        return ' sftp';
    }
    return ' ftp';
}


/**
 * Get password
 * @param $ftp_passwd string the ftp password
 * @param string $sOutputType string console for cmd, display, meaning the password will be displayed as ######
 * @param boolean $blEscape true to escape special characters, false returns the password unescaped
 * @return string
 */
function getPassword($ftp_passwd, $sOutputType='console', $blEscape = true)
{
    if($sOutputType == 'console'){
        if($blEscape){
            //escape the special characters
            return preg_quote($ftp_passwd);
        }else{
            return $ftp_passwd;
        }
    }
    return '#################';
}


/**
 * Get password
 * @param $ftp_user string the ftp user
 * @param boolean $blEscape true to escape special characters, false returns the password unescaped
 * @return string
 */
function getUser($ftp_user, $blEscape = true)
{
    if($blEscape){
        //escape the special characters
        return preg_quote($ftp_user);
    }
    return $ftp_user;

}


/**
 * Create the lftp command
 * @param $transfer_direction string transfer direction, push or pull
 * @param $connection_type string connect type sftp (default) or ftp
 * @param $ftp_user string ftp user
 * @param $ftp_passwd string ftp password
 * @param $ftp_server string ftp server
 * @param $ftp_path string ftp path / folder
 * @param $localPath string local path
 * @param string $sOutputType  string console for cmd, display, meaning the password will be displayed as ######
 * @return string
 */
function getCommand($transfer_direction, $connection_type, $ftp_user, $ftp_passwd, $ftp_server, $ftp_path, $localPath, $sOutputType='console')
{
    $cmd = '';
    $dPort = getPort($connection_type);
    $sConnection_type = getConnectionString($connection_type);
    $sPasswordEscaped = getPassword($ftp_passwd, $sOutputType);
    $sUserEscaped = getUser($ftp_user);

    if($transfer_direction == 'push'){
        //Datei/Ordner an den FTP-Server verschicken
        $cmd .= "lftp -u $sUserEscaped,$sPasswordEscaped -p $dPort" ;  //source_path_abs
        $cmd .= ' -e "mirror -R --parallel=3 '. $localPath . ' ' . $ftp_path  . '"';
        $cmd .= "$sConnection_type://$ftp_server";
    }else if ($transfer_direction == 'pull'){
        //PULL - Datei/Ordner vom angegebenen FTP-Server ziehen
        $cmd .= "lftp -u $sUserEscaped,$sPasswordEscaped -e ";  //source_path_abs
        $cmd .= '"mirror --parallel=3 ' . $sConnection_type . '://' . $ftp_user . ':' .  getPassword($ftp_passwd, $sOutputType, false)  . '@' ;
        $cmd .=  $ftp_server . ':' . $dPort . $ftp_path . '"';
        $cmd .=  "$sConnection_type://$ftp_server";
    }

    return $cmd;
}

ob_start();
switch ($fnc) {
    case 'transfer':
        echo '<div class="output">';
        //lftp command
        $cmd =  getCommand($_REQUEST['transfer_direction'], $_REQUEST['connection_type'], $_REQUEST['ftp_user'], $_REQUEST['ftp_passwd'], $_REQUEST['ftp_server'], $_REQUEST['ftp_path'], $_REQUEST['local_path']) ;
        $cmd .=  getOutput($sPROCEESSERRORLOGFILE) .' & echo $! > '.  $sPROCEESSPIDFILE;
        $sDisplayCmd = getCommand($_REQUEST['transfer_direction'], $_REQUEST['connection_type'], $_REQUEST['ftp_user'], $_REQUEST['ftp_passwd'], $_REQUEST['ftp_server'], $_REQUEST['ftp_path'], $_REQUEST['local_path'], 'display') ;

        echo "<p class='cmd'>$sDisplayCmd</p>";

        //get old pid and check if it is still running
        $oldProcessRunning = getLastRunStatus($sPROCEESSPIDFILE, false, false);

        //run command
        if($oldProcessRunning){
            echo "<p>Old lftp process still running .....</p>";
        }else{
            //for background . " > /dev/null &"
            //for background and log pid to file  '> /dev/null & echo $! > lftp.pid'

            $out = array();
            exec ($cmd, $out,$ret);  ///dev/null
            if(count($out)){
                echo htmlentities(implode(PHP_EOL,$out), ENT_QUOTES, 'UTF-8');
                echo "<p>DONE.</p>";
                //deleted all help files created
                deleteFile(array($sPROCEESSPIDFILE, $sPROCEESSERRORLOGFILE));
            }else{
                echo "processing ...";
            }
        }
        echo '</div>';
        break;

    case 'status_check':
        echo '<div class="output">';
        $oldProcessRunning = getLastRunStatus($sPROCEESSPIDFILE, true);
        if($oldProcessRunning){
            echo "<p>Old process still running .....</p>";
        }else{
            $sPROCESSSTATE = 'COMPLETED';
            //check error log
            $success = isSuccessful($sPROCEESSERRORLOGFILE);

            //deleted all help files created
            deleteFile(array($sPROCEESSPIDFILE, $sPROCEESSERRORLOGFILE));
            echo "</br><p>last lftp process completed.</p>";

            //Delete script
            if($success && isset($_REQUEST['settings_delete_script'])){
                deleteFile(array('lftp.php'));
                echo "</br><p class='centerparagraph'>This page is dead † " . date("j. F, Y H:i:s") . getLocation() . "</p>";
            }
        }
        echo '</div>';
        break;

    default:
        break;

}
$output = ob_get_clean();
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>LFTP Dateitransfer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <style type="text/css">
        html, body { background: #3d3b38; color: #fff; margin: 0; padding: 0; }
        html { min-height: 100%; margin-bottom: 0.1px; border-bottom: 1px solid transparent; }

        .lftp_container { clear: both; margin: 35px auto; padding: 20px; color: #3d3b38; background: #fff; font: 100.01% Tahoma, Arial, sans-serif; max-width : 810px;}

        .output {background-color: black;color:white; padding: 5px 5px 5px 5px;}
        .cmd  {color:yellow; }
        .success  {color:green; }
        .error  {color:red; }
        .log {margin: 10px; padding: 10px; border: 1px solid orange;}
        .centerparagraph{ text-align: center; }

        h2 { margin: 0 0 20px 0; font-weight: 400; }
        pre { margin: 0 0 20px 0; white-space: pre-wrap;  /* Since CSS 2.1 */ white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */  white-space: -pre-wrap;  /* Opera 4-6 */  white-space: -o-pre-wrap;    /* Opera 7 */  word-wrap: break-word;       /* Internet Explorer 5.5+ */}
        fieldset { margin: 0 0 30px 0; padding: 10px 20px 0 20px; border: 1px solid #d9d7d2; }
        .fieldsetRow { margin-bottom:20px;}
        legend { padding: 0 15px; font-weight: 600; font-size: 14px; }

        label.req:after {
            content: '*';
            color: #f84c60;
        }

        .description, span {padding-left:5px; margin: 5px 0; font-size: 12px;  font-family: "Arial";}

        input[type=text], input[type=password], select, option {width:100%;border:1px solid #cccccc;height: 34px;padding: 6px 12px;font-size: 14px; border-radius: 4px;}

        button[type=submit] { -webkit-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0; -moz-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0; box-shadow:rgba(0,0,0,0.2) 0 1px 0 0; border-bottom-color:#333; border:1px solid #61c4ea; background-color:#7cceee; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; color:#333; font-family:'Verdana',Arial,sans-serif; font-size:14px; text-shadow:#b2e2f5 0 1px 0; padding:5px; }

        .radio { float:left; width:100%; margin-left: 20px; margin-top:-10px; }
        
        .lineBox { width: 100%; float: left; clear: both; margin: 0 0 20px 0; }
        .lineBoxBottom { margin: 0; }
        .lineBoxTop { }
        .lineBox h2, .lineBox .metaButton { float: left; margin: 0; }
        .lineBox .submitButton { float: right; margin: 0; }

        .clear { clear: both; }

    </style>
    <script type="text/javascript">
        $(function(){

            /**
             * trigger the check process status
             */
            var checkProcessState = function(){
                var scriptCalledOnce = "<?php echo $fnc; ?>";
                if(scriptCalledOnce != ""){
                    $('#checkProcessState').trigger('click');
                }
            };
            //retrigger only when the process is still running
            if("<?php echo $sPROCESSSTATE; ?>" != "COMPLETED"){
                setTimeout(checkProcessState, 60000);
            }


            /**
             * remove GET params from url once loaded …
             */
            $(window).ready(function(){
                window.history.pushState({}, document.title, window.location.href.split( '?' )[0]);
            })


            /**
             * changes elements based on the chosen transfer direction
             */
            $('input[name="transfer_direction"]').on('change', function(){

                var checked_input = $('input[name="transfer_direction"]:checked'),
                    checked_value = checked_input.attr('value'),
                    checked_data = 'data-'+checked_value;

                // switch texts by data-attributes
                $('*['+checked_data+']').each(function(){
                    $(this).html( $(this).attr(checked_data) );
                });

                //switch fieldset position
                if (checked_value == 'push') {
                    $("#ftp").before($("#local"));
                    $("#local_path").prop("readonly", false);
                } else if (checked_value == 'pull') {
                    $("#local").before($("#ftp"));
                    $("#local_path").prop("readonly", true);
                    $("#local_path").prop("value", "<?php echo getcwd(); ?>");
                }

            }).trigger('change');


            /**
             * write selected option to input
             */
            $('#sel_local_path').on('change', function(){
                if($(this).val() != "Verzeichnis auswählen"){
                    $( "#local_path" ).prop("value",  $(this).val() );
                    $("#local_path" ).html( $(this).val() );
                    $("#local_path").css('border-color', '#cccccc');
                }
            });

            /**
             * ftp path select change event handler
             */
            $('#sel_ftp_path').on('change', function(){
                ftpDirectorySelectChangeEventHandler($(this));
            });


            /**
             * ftp path input blur event handler
             * performs the same action as the select change event
             */
            $( "#ftp_path" ).on('blur', function(){
                //recall directory getContent
                loadFTPDirSubDir();
            });

            /**
             * ftp path input blur event handler
             * performs the same action as the select change event
             */
            $( "#ftp_passwd" ).on('blur', function(){
                //recall directory getContent
                loadFTPDirSubDir();
            });

            /**
             * ftp path input blur event handler
             * performs the same action as the select change event
             */
            $( "#ftp_user" ).on('blur', function(){
                //recall directory getContent
                loadFTPDirSubDir();
            });


            /**
             * ftp path input blur event handler
             * performs the same action as the select change event
             */
            $( "#ftp_server" ).on('blur', function(){
                //recall directory getContent
                loadFTPDirSubDir();
            });


            /**
             * validate inputs on submit
             */
            $( "#lftpform" ).submit(function( event ) {
                //submit on status check
                if( $('input[name="fnc"]').val() == 'status_check'){
                    return;
                }

                //validate form
                var valid = true;
                $('form#lftpform label.req').each(function(){
                    var inputId = $(this).attr('for');
                    var input = $('input#' + inputId);
                    if($.trim(input.val()) == ''){
                        valid = false;
                        $('input[type=text]#' + inputId).css('border-color', 'red');
                    }else{
                        $('input[type=text]#' + inputId).css('border-color', '#cccccc');
                    }

                });

                if(valid){
                    //submit form
                    return;
                }
                //prevent from submit
                event.preventDefault();
            });


            /**
             * help function for the ftp path.
             * reloads the pages
             */
            var loadFTPDirSubDir = function(){
                if($.trim($( "#ftp_server" ).val()) != '' && $.trim($( "#ftp_user" ).val()) != '' && $.trim($( "#ftp_passwd" ).val()) != ''){
                    $( "#ajaxCall" ).attr('value', "true");
                    jQuery.ajax({
                        type: "POST",
                        data:  $( "form#lftpform" ).serialize(),
                        success: function(data) {
                            $( "#ajaxCall" ).attr('value', "false");
                            $('#sel_ftp_path').replaceWith($(data).find('#sel_ftp_path'));

                            //bind back functions
                            $('#sel_ftp_path').on('change', function(){
                                ftpDirectorySelectChangeEventHandler($(this));
                            });

                            // Check the output of ajax call on firebug console
                            console.log($(data).find('#sel_ftp_path'));
                        }
                    });
                }
            }


            /**
             * help function: ftp path select change
             * @param object select element
             */
            var ftpDirectorySelectChangeEventHandler = function(object)
            {
                if(object.val() != "Verzeichnis auswählen"){
                    $( "#ftp_path" ).prop("value",  object.val() );
                    loadFTPDirSubDir();
                }
            }


            /**
             * show password functionality
             */
            $(".showpassword").each(function(index,input) {
                var $input = $(input);
                $('<label />').append(
                    $("<input type='checkbox' />").click(function() {
                        var change = $(this).is(":checked") ? "text" : "password";
                        var rep = $("<input type='" + change + "' />")
                            .attr("id", $input.attr("id"))
                            .attr("name", $input.attr("name"))
                            .attr('class', $input.attr('class'))
                            .attr('placeholder', $input.attr('placeholder'))
                            .val($input.val())
                            .insertBefore($input);
                        $input.remove();
                        $input = rep;
                    })
                ).append($("<span/>").text("Passwort Anzeigen")).insertAfter($input);
            });

        });
    </script>
</head>
<body>
   <div class="container lftp_container">

        <?php if (strlen(trim($output)) > 0) { ?>
            <pre id="output"><?php echo $output;?></pre>
        <?php } ?>

        <form id="lftpform" name="lftpform" method="post" action="#">
            <input type="hidden" name="fnc" value="transfer">
            <input type="hidden" name="ajaxCall" id="ajaxCall" value="false">

            <div class="lineBox lineBoxTop">
                <h2>LFTP Dateitransfer</h2>
                <button class="submitButton" type="submit">Übertragen</button>
            </div>

            <div class="clear"></div>

            <fieldset>
                <legend>Ausführungsrichtung</legend>
                <p class="radio">
                    <input type="radio" id="pull" name="transfer_direction" value="pull" <?php echo $pull_direction; ?>> <label for="pull">PULL - Datei/Ordner vom angegebenen FTP-Server ziehen</label>
                </p>
                <p class="radio">
                    <input type="radio" id="push" name="transfer_direction" value="push" <?php echo $push_direction; ?>> <label for="push">PUSH - Datei/Ordner an den FTP-Server verschicken</label>
                </p>
            </fieldset>

            <fieldset>
                <legend>Verbindungsart</legend>
                <p class="radio">
                    <input type="radio" id="sftp_conn_type" name="connection_type" value="sftp_conn_type" <?php echo $sftp_connection; ?> >
                    <label for="sftp">SFTP-Verbindung</label>
                </p>
                <p class="radio">
                    <input type="radio" id="ftp_conn_type" name="connection_type" value="ftp_conn_type" <?php echo $ftp_connection; ?> >
                    <label for="ftp_conn_type">FTP-Verbindung</label>
                </p>
            </fieldset>

            <fieldset id="ftp">
                <legend data-push="Ziel - FTP-Daten" data-pull="Quelle -FTP-Daten">FTP Daten</legend>
                <div class="row fieldsetRow">
                    <div class="col-sm-4">
                        <label class="req" for="ftp_user">FTP-User</label>
                    </div>
                    <div class="col-sm-8">
                        <input id="ftp_user" type="text" placeholder="FTP User" name="ftp_user" value="<?php echo $ftp_user; ?>">
                    </div>
                </div>
                <div class="row fieldsetRow">
                    <div class="col-sm-4">
                        <label class="req" for="ftp_passwd">FTP-Passwort</label>
                    </div>
                    <div class="col-sm-8">
                        <input class="showpassword" id="ftp_passwd" type="password" placeholder="FTP Password" name="ftp_passwd" value="<?php echo $ftp_passwd; ?>">
                    </div>
                </div>

                <div class="row fieldsetRow">
                    <div class="col-sm-4">
                        <label class="req" for="ftp_server">FTP-Server</label>
                    </div>
                    <div class="col-sm-8">
                        <input id="ftp_server" type="text" placeholder="FTP Server" name="ftp_server" value="<?php echo $ftp_server; ?>">
                    </div>
                </div>

                <div class="row fieldsetRow">
                    <div class="col-sm-4">
                        <label class="req" for="ftp_path" data-pull="FTP-Pfad zur Quell-Datei/Ordner" data-push="FTP-Pfad zum Ziel-Ordner">Relativer FTP-Pfad</label>
                    </div>
                    <div class="col-sm-8">
                        <input id="ftp_path" placeholder="FTP Folder" title="Dieses Verzeichnis empfängt das [Download / Export Verzeichnis]" type="text" name="ftp_path" value="<?php echo $ftp_path; ?>">
                        <p class="description" data-push="Relative Pfad, wo die Datei bzw. den Ordner hinein geschickt wird" data-pull="Pfad ins FTP-Quell-Verzeichnis"></p>
                        <div class="select">
                            <select name="sel_ftp_path" id="sel_ftp_path">
                                <option>Verzeichnis auswählen</option>
                                <?php
                                $directories = getFTPDirContent($ftp_user, $ftp_passwd, $ftp_server, $ftp_path);
                                foreach($directories as $dir) { ?>
                                    <option value="<?php echo $dir ?>" <?php echo ($dir == $ftp_path)? 'selected="selected"' : ''?> title="<?php echo $dir ?>"><?php echo $dir ?></option>
                                    <?php
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset id="local">
                <legend data-push="Quelle - Ordner/Datei auf aktuellem Server" data-pull="Ziel - Ordner auf aktuellem Server"></legend>
                <div class="row fieldsetRow">
                    <div class="col-sm-4">
                        <label class="req" for="local_path" data-pull="Pfad zum Ziel-Ordner" data-push="Pfad zur Quell-Datei/Ordner ">Pfad</label>
                    </div>
                    <div class="col-sm-8">
                        <input id="local_path" title="Dieses Verzeichnis wird im [Relative Pfad zum Zielverzeichnis] hochgeladen." placeholder="Source / Target local Folder" type="text" name="local_path" value="<?php echo $local_path; ?>">
                        <p class="description" data-push="Absoluter Pfad, der die Daten zum Verschicken enthält." data-pull="In diesem Verzeichnis werden die Daten aus dem FTP-Server hinein gezogen. </br>In dem Fall muss das Skript von hier gestartet werden."></p>
                        <div id="sel_ftp_div">
                            <select name="sel_local_path" id="sel_local_path">
                                <option >Verzeichnis auswählen</option>
                                <?php
                                $directories = getLocalDirContents ($local_path);
                                foreach($directories as $dir) { ?>
                                    <option value="<?php echo $dir ?>" <?php echo ($dir == $local_path)? 'selected="selected"' : ''?> title="<?php echo $dir ?>"><?php echo $dir ?></option>
                                    <?php
                                } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset id="settings">
                <legend>Einstellungen</legend>
                <p class="radio">
                    <input id="settings_delete_script" type="checkbox" name="settings_delete_script" value="delete_script" <?php echo $delete_script ?> >
                    <label for="settings_delete_script">Die Skript bzw. die Seite nach einer erfolgreichen Übertragung löschen</label>
                </p>
            </fieldset>

            <div class="lineBox lineBoxBottom">
                <button id="checkProcessState" class="metaButton" onclick="javascript:document.forms['lftpform'].elements['fnc'].value='status_check';" type="submit">Status Abfragen</button>
                <button class="submitButton" type="submit" >Übertragen</button>
            </div>

            <div class="clear"></div>
        </form>

    </div>

</body>
</html>

